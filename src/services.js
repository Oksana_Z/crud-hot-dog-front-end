const baseURL = 'http://localhost:3000'
export default {
  fetchSandwiches () {
    return fetch(`${baseURL}/sandwiches/`, {
      method: 'GET'
    })
      .then((text) => text.json())
  },
  createSandwich (formData) {
    return fetch(`${baseURL}/sandwiches/`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: formData
    })
      .then((text) => text.json())
  },
  updateSandwich (updatedSandwich) {
    return fetch(`${baseURL}/sandwiches/${updatedSandwich._id}`, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(updatedSandwich)
    })
      .then((text) => text.json())
  },
  deleteSandwich (deleteSandwich) {
    return fetch(`${baseURL}/sandwiches/${deleteSandwich._id}`, {
      method: 'DELETE'
    })
  }
}
